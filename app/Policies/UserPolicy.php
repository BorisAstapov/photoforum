<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;


class UserPolicy
{
    use HandlesAuthorization;

    public function viewRoles(User $user)
    {
        $is_admin = $user->role === 'admin';
        $is_manager = $user->role === 'manager';

        return ($is_admin || $is_manager);
    }

    public function setRoles(User $user)
    {
        $is_admin = $user->role === 'admin';
        return $is_admin;
    }
}
