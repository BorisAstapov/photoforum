<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }

    public function show(Article $user)
    {
        return $user;
    }

    public function store(Request $request)
    {
        $article = User::create($request->all());

        return response()->json($user, 201);
    }

    public function update(Request $request, User $user)
    {
        $article->update($request->all());

        return response()->json($user, 200);
    }

    public function delete(User $user)
    {
        $article->delete();

        return response()->json(null, 204);
    }

    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }
}
