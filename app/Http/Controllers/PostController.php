<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


use App\Models\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    //function that sets name for image file
    private function renameImage($file,$salt){
        #$name = $file->getClientOriginalName();
        $name = now()->format('Ymdhis');
        $name .= $salt;
        $extension = $file->extension();
        return $name.'.'.$extension;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'name' => ['required'],
        ]);

        if ($request->file('file')->isValid()) {

            $id = Auth::id();
            $name = $this->renameImage($request->file('file'),$id);
            $destination = "/".$id."/{$name}";
            
            $path = $request->file->storeAs(
                'public', 
                $destination
            );

            Auth::user()->posts()->create([
                'name' => $input['name'],
                'file' => $destination,
            ]);

            return back()->with('success', "succesfully uploded");   
        }

        return back()->withErrors([
            'general' => 'The provided credentials do not match our records.',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if (Auth::user()->cannot('delete', $post)) {
            return back()->withErrors([
                'general' => 'Cannot delete that post',
            ]);
        }
        Storage::delete("public/".$post->file);
        $post->delete();
        return back()->with('success', "succesfully deleted");   
    }
}
