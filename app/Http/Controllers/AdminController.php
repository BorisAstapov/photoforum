<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

class AdminController extends Controller
{
    public function roleView(Request $request){

        $users = User::paginate(15);
        return view('admin.role',[
            'users' => $users,
        ]);
    }

    public function setRole(Request $request){
        try{
            
            if ($request->user()->cannot('setRoles',User::class)) {
                return back()->withErrors([
                    'setError' => 'not authorized to do that',
                ]);
            }

            $id = $request->input('id');            
            $user = User::findOrFail($id);
            $user->role = $request->input('role');
            $user->save();

            return back()->with('success', "role of user {$user->user_name} set to {$user->role}");   
        }
        catch(Throwable $e){
            return back()->withErrors([
                'setError' => $e->getMessage(),
            ]);
        }

    }
    
}
