<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;

use App\Models\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# Auth Routes
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
Route::get('/login', [AuthController::class, 'loginForm'])->name('login');
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/register', [AuthController::class, 'registrationForm'])->name('register');
Route::post('/register', [AuthController::class, 'registration'])->name('register');

#Admin Routes
Route::prefix('admin')->group(function () {
    
    Route::get('/roles', [AdminController::class, 'roleView']);
    Route::post('/set_role', [AdminController::class, 'setRole'])->name('set_role');

});

#User Routes
Route::prefix('user')->group(function () {
    
    Route::get('/list', [UserController::class, 'index']);
    Route::get('/{id}', [UserController::class, 'show']);

});

#Post Routes
Route::resource('posts', PostController::class);

Route::get('/', function () {
    $posts = Post::orderBy('created_at')
    ->take(4)
    ->get();
    return view('welcome',[ 'posts' => $posts]);
});
