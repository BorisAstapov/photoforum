<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

use App\Models\Post;
use App\Models\User;

class PostTest extends TestCase
{
    use RefreshDatabase;

    private function create_file($user){

        $file = UploadedFile::fake()->image('image.jpg');

        $response = $this->actingAs($user)->post('/posts', [
            'name' => 'image_name',
            'file' => $file,
        ]);

        $user->refresh();
    }

    public function test_file_upload(){
        
        Storage::fake('local');
        $user = User::factory()->create();
        $this->create_file($user);

        $this->assertCount(1,Post::all());

        $post = Post::first();
        $this->assertEquals($user->id,$post->user_id);
        $this->assertEquals($post,$user->posts()->first());
        Storage::disk('local')->assertExists('public/'.$post->file);
    }

    public function test_delete_file(){
        
        Storage::fake('local');
        $user = User::factory()->create();
        $this->create_file($user);

        $post = Post::first();
        $response = $this->actingAs($user)->delete('/posts/'.$post->id);
        $user->refresh();

        $response->assertSessionHas( 'success', 'succesfully deleted');
        $this->assertEmpty(Post::all());
        Storage::disk('local')->assertMissing('public/'.$post->file);
    }

    public function test_only_creator_can_delete_file(){
        
        Storage::fake('local');
        $user = User::factory()->create();
        $this->create_file($user);

        $post = Post::first();
        $different_user = User::factory()->create();
        $response = $this->actingAs($different_user)->delete('/posts/'.$post->id);
        $response->assertSessionHasErrors([
            'general' => 'Cannot delete that post',
        ]);
        $this->assertCount(1,Post::all());
        Storage::disk('local')->assertExists('public/'.$post->file);
    }

}
