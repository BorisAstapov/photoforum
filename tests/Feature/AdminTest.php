<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;

class AdminTest extends TestCase
{
    use RefreshDatabase;

    public function test_rolelist_visible_as_admin(){
        
        $user = User::factory()->create();
        $user->role = 'admin';
        $user->save();
        
        $view = $this->actingAs($user)->view('welcome', [ 'posts' => [] ]);

        $view->assertSee('Roles');
    }

    public function test_rolelist_invisible_as_user(){
        
        $user = User::factory()->create();
        
        $view = $this->actingAs($user)->view('welcome', [ 'posts' => [] ]);
        
        $view->assertDontSee('Roles');
    }
}
