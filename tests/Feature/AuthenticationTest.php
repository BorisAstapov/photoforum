<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;
/*
    public function test_correct_login(){
        
        $user = User::factory()->create();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $response->assertSessionHasNoErrors();
        $this->assertAuthenticated($guard = null);
        $response->assertRedirect('');
    }

    public function test_empty_login(){
        $response = $this->post('/login', [
            'email' => '',
            'password' => ''
        ]);

        $response->assertSessionHasErrors([
            'email', 'password',
        ]);
        $this->assertGuest($guard = null);
    }

    public function test_incorrect_login(){

        $response = $this->post('/login', [
            'email' => 'notrealemail@email.com',
            'password' => 'wrongpassword'
        ]);

        $response->assertSessionHasErrors([
            'email' => 'The provided credentials do not match our records.'
        ]);
        $this->assertGuest($guard = null);
    }

    public function test_correct_registration(){
        $response = $this->post('/register', [
            'user_name' => 'testuser',
            'email' => 'testmail@mail.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        $response->assertSessionHasNoErrors();
        $this->assertCount(1,User::all());
        $this->assertAuthenticated($guard = null);
    }

    public function test_empty_registration(){
        $response = $this->post('/register', [
            'user_name' => '',
            'email' => '',
            'password' => '',
        ]);
        $response->assertSessionHasErrors([
            'user_name','email','password'
        ]);
        $this->assertCount(0,User::all());
        $this->assertGuest($guard = null);
    }
*/

}
