<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_attributes_casting(){

        User::factory()->create();
        $user = User::first();
        $user->birthday = now();
        $user->save();

        $this->assertIsString($user->user_name);
        $this->assertIsString($user->email);
        $this->assertIsString($user->role);
        $this->assertInstanceOf(Carbon::class, $user->birthday);

    }
}
