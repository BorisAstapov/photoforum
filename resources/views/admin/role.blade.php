@extends('layouts.app')

@section('page_title')
    Manage Roles
@endsection

@section('content')

@error('setError')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

@if (\Session::has('success'))
    <div class="alert alert-success">{{ \Session::get('success') }} </div>
@endif

<div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
    <a href="{{ url('/') }}" class="text-sm text-gray-700 underline">Home</a>
    <a href="{{ route('logout') }}" class="text-sm text-gray-700 underline">Log out</a>

</div>

<table class="User">
    <thead>
        <tr>
            <td><strong>User name</strong></td>
            <td>Role</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
            <tr>
                <td>{{ $user->user_name }}</td>

                <td>
                    <form method="POST" action="{{ route('set_role') }}">
                        @csrf
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <select name="role">
                            <option value="user" @if ($user->role === 'user') selected @endif>user</option>
                            <option value="manager" @if ($user->role === 'manager') selected @endif>manager</option>
                            <option value="admin" @if ($user->role === 'admin') selected @endif>admin</option>
                        </select>
                        <input type="submit" value="save">
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

{{ $users->links() }}

@endsection