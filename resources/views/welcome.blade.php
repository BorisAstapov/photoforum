@extends('layouts.app')

@section('page_title')
    PhotoForum
@endsection

@section('content')
    <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
        @if (Route::has('login'))
            <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            <a href="{{ url('/user/list') }}" class="text-sm text-gray-700 underline">Users</a>
                @auth
                @can('viewRoles',App\Models\User::class)
                    <a href="{{ url('/admin/roles') }}" class="text-sm text-gray-700 underline">Roles</a>
                @endcan
                    <a href="{{ url('/user/').'/'.Auth::user()->id }}" class="text-sm text-gray-700 underline">My account</a>
                    <a href="{{ route('logout') }}" class="text-sm text-gray-700 underline">Log out</a>
                @else
                    <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Log in</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                    @endif
                @endauth
            </div>
        @endif

        @forelse  ($posts as $post)
            <img src="{{ asset('storage'.$post->file) }}" alt="{{ $post->name }}" width="500" height="600"> 
        @empty
            <p>No posts</p>
        @endforelse 
    </div>
@endsection
