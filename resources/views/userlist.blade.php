@extends('layouts.app')

@section('page_title')
    User List
@endsection

@section('content')

@error('setError')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
    <a href="{{ url('/') }}" class="text-sm text-gray-700 underline">Home</a>
    <a href="{{ route('logout') }}" class="text-sm text-gray-700 underline">Log out</a>

</div>

<div class="User">
    <div>
            <h3><strong>User name</strong></h3>
    </div>
    <div>
        @foreach ($users as $user)

        <div>
            <a href="{{ url('/user/').'/'.$user->id }}"><p>{{ $user->user_name }}</p>        </a>
        </div>

        @endforeach
    </div>
</div>

{{ $users->links() }}

@endsection