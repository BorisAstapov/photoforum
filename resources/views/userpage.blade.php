@extends('layouts.app')

@section('page_title',$user->user_name)
    
@error('general')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

@if (\Session::has('success'))
    <div class="alert alert-success">{{ \Session::get('success') }} </div>
@endif

@section('content')
<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
    
    @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            <a href="{{ url('/user/list') }}" class="text-sm text-gray-700 underline">Users</a>
            @auth
                <a href="{{ url('/') }}" class="text-sm text-gray-700 underline">Home</a>
                <a href="{{ route('logout') }}" class="text-sm text-gray-700 underline">Log out</a>
            @else
                <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Log in</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <h1>{{ $user->user_name }}</h1>

    <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="text" name="name" placeholder="image name" required ><br>
        <input type="file" name="file" required ><br>
        <input type="submit">
    </form>
    
    @forelse  ($posts as $post)
        <div>
            <img src="{{ asset('storage'.$post->file) }}" alt="{{ $post->name }}" width="250" height="250"> 
            @can('delete',$post)
            <form action="{{ route('posts.destroy',[$post->id]) }}" method="POST">
                @method('DELETE')
                @csrf
                <button type="submit">Delete</button>               
            </form>
            @endcan
        </div>
    @empty
        <p>No posts</p>
    @endforelse 
</div>

@endsection