@extends('layouts.app')

@section('page_title')
    Login
@endsection

@section('content')
<div class="relative flex flex-column items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">

    @if (session('status'))
        <div>
            {{ session('status') }}
        </div>
    @endif

    @if ($errors->any())
        <div>
            <div>Whoops! Something went wrong.</div>

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div>
            <label>Email</label>
            <input type="email" name="email" value="{{ old('email') }}" required autofocus />
        </div>

        <div>
            <label>Password</label>
            <input type="password" name="password" required autocomplete="current-password" />
        </div>

        <div>
            <label>Remember me</label>
            <input type="checkbox" name="remember">
        </div>

        @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}">
                Forgot your password?
            </a>
        @endif

        <div>
            <button type="submit">
               Login
            </button>
        </div>
    </form>
<div>
@endsection