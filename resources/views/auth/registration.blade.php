@extends('layouts.app')

@section('page_title')
    Registration
@endsection

@section('content')
<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
    @if ($errors->any())
        <div>
            <div>'Whoops! Something went wrong.'</div>

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('register') }}">
        @csrf

        <div>
            <label>Name</label>
            <input type="text" name="user_name" value="{{ old('user_name') }}" required autofocus autocomplete="user_name" />
        </div>

        <div>
            <label>Email</label>
            <input type="email" name="email" value="{{ old('email') }}" required />
        </div>

        <div>
            <label>Password</label>
            <input type="password" name="password" required autocomplete="new-password" />
        </div>

        <div>
            <label>Confirm Password'</label>
            <input type="password" name="password_confirmation" required autocomplete="new-password" />
        </div>

        <div>
            <label>Bithday</label>
            <input type="date" name="birthday"/>
        </div>

        <a href="{{ route('login') }}">
            Already registered?
        </a>

        <div>
            <button type="submit">
                Register
            </button>
        </div>
    </form>
<div>
@endsection